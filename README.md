# Software Engineering 2020 - Individual Project - CollectionTracker

## 1806241021 - Aljihad Ijlal Nadhif Suyudi

This is a repository for CollectionTracker application.

## How to run the program
1. Download the files from this repository
2. Navigate to the path that contains "CollectionTracker2.jar"
<...\CollectionTracker\out\artifacts\CollectionTracker2_jar>
3. Open your terminal, and type "java -jar CollectionTracker2.jar"
4. The program will be running shortly. Input "help" first to know all commands in it.

For see further documents, please click on links below:
 - Works Document - https://docs.google.com/document/d/1lgK9g2yarLPnhBr_Tn4_697GroNUa20N71hHhtY08iQ/edit?usp=sharing
 - Works Log - https://docs.google.com/spreadsheets/d/1xagbjvdf9EsRRDqIq7kKZhI5cTtnrQz3eCbTEdA1R1I/edit?usp=sharing
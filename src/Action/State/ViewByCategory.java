package Action.State;

import InputReader.InputReader;
import Model.Objects;
import Repository.AllObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ViewByCategory implements State{

    private InputReader in = new InputReader(System.in);
    private String inp;

    @Override
    public void doAction() {
        HashMap current = AllObject.getRepo();

        System.out.println("Please input the object category you want to see: ");
        inp = in.next();

        Iterator currIterator = current.entrySet().iterator();

        while (currIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)currIterator.next();
            Objects target = (Objects) mapElement.getValue();
            if(target.getCategory().equalsIgnoreCase(inp)){
                System.out.println(target);
                System.out.println("");
            }
        }
    }
}

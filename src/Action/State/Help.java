package Action.State;

public class Help implements State{
    @Override
    public void doAction() {
        System.out.println("\n==============:LIST OF COMMANDS:==============");
        System.out.println(" store : store information about your desired object.");
        System.out.println(" modify : modify information on object that has been stored.");
        System.out.println(" view : view an object information that you want to see.");
        System.out.println(" viewAll : view all object that you have been stored.");
        System.out.println(" viewRarity : view all object based on its rarity.");
        System.out.println(" viewCategory : view all object based on its category.");
        System.out.println(" help : show this list of commands.");
        System.out.println(" exit : quits the application.\n");
    }
}

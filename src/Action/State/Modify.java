package Action.State;

import Action.Command.*;
import InputReader.InputReader;
import Model.Objects;
import Repository.AllObject;

public class Modify implements State{
    private InputReader in = new InputReader(System.in);
    private String inp;
    private boolean valid = false;
    private CommandContext cmd = new CommandContext();
    private StoreName name = new StoreName();
    private StoreDate date = new StoreDate();
    private StoreYear year = new StoreYear();
    private StoreRarity rarity = new StoreRarity();
    private StorePlace place = new StorePlace();
    private StoreCategory category = new StoreCategory();


    @Override
    public void doAction() {
        System.out.println("Please input the name of the object that you want to modify: ");
        inp = in.next();

        if (AllObject.search(inp)==null){
            System.out.println("Please store an object first!");
        } else {
            Objects object = AllObject.search(inp);
            AllObject.remove(inp);

            do {
                System.out.println("Which information that you want to modify? : ");
                System.out.println("Please input the number from list below: ");
                System.out.println("1. Name of the object\n" + "2. Date of bought\n" +
                        "3. Manufactured year\n" + "4. Rarity of the object\n" +
                        "5. Place where you bought it\n" + "6. Category of the object\n");
                inp = in.next();

                if (inp.equals("1")){
                    cmd.setState(name);
                    cmd.command(object);
                    AllObject.add(object);
                    valid = true;
                } else if( inp.equals("2")){
                    cmd.setState(date);
                    cmd.command(object);
                    AllObject.add(object);
                    valid = true;
                }else if( inp.equals("3")){
                    cmd.setState(year);
                    cmd.command(object);
                    AllObject.add(object);
                    valid = true;
                }else if( inp.equals("4")){
                    cmd.setState(rarity);
                    cmd.command(object);
                    AllObject.add(object);
                    valid = true;
                }else if( inp.equals("5")){
                    cmd.setState(place);
                    cmd.command(object);
                    AllObject.add(object);
                    valid = true;
                } else if(inp.equals("6")){
                    cmd.setState(category);
                    cmd.command(object);
                    AllObject.add(object);
                    valid = true;
                }else {
                    System.out.println("You put a wrong number! Please look at the list again!\n");
                }
            }while (!valid);
            System.out.println("The object has been modified!");
        }

    }
}

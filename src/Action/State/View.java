package Action.State;

import InputReader.InputReader;
import Repository.AllObject;

public class View implements State{
    private InputReader in = new InputReader(System.in);
    private String inp;

    @Override
    public void doAction() {

        System.out.println("Please input the name of the object: ");
        inp = in.next();

        System.out.println("");

        if(AllObject.search(inp) != null){
            System.out.println(AllObject.search(inp));
            System.out.println("");
        }

    }
}

package Action.State;

import Action.Command.*;
import Model.Objects;
import Repository.AllObject;

public class Store implements State {
    private CommandContext cmd = new CommandContext();
    private StoreName name = new StoreName();
    private StoreDate date = new StoreDate();
    private StoreYear year = new StoreYear();
    private StoreRarity rarity = new StoreRarity();
    private StorePlace place = new StorePlace();
    private StoreCategory category = new StoreCategory();


    @Override
    public void doAction() {
        Objects object = new Objects();

        cmd.setState(name);
        cmd.command(object);

        cmd.setState(date);
        cmd.command(object);

        cmd.setState(year);
        cmd.command(object);

        cmd.setState(rarity);
        cmd.command(object);

        cmd.setState(place);
        cmd.command(object);

        cmd.setState(category);
        cmd.command(object);

        AllObject.add(object);

        System.out.println("\nThe object has been stored!");
    }

}

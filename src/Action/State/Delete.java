package Action.State;

import InputReader.InputReader;
import Repository.AllObject;

public class Delete implements State{
    private InputReader in = new InputReader(System.in);
    private String inp;

    @Override
    public void doAction() {
        System.out.println("Please input the name of the object that you want to delete: ");
        inp = in.next();

        AllObject.remove(inp);

        System.out.println("The object has been deleted!");
    }
}

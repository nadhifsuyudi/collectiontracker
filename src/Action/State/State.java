package Action.State;

public interface State {

    public void doAction();
}

package Action.State;

import Repository.AllObject;

import java.util.HashMap;

public class ViewAll implements State{
    @Override
    public void doAction() {
        HashMap current = AllObject.getRepo();

        current.forEach((k, v)-> System.out.println(v + "\n"));
    }
}

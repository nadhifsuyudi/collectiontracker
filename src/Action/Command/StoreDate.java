package Action.Command;

import InputReader.InputReader;
import Model.Objects;

public class StoreDate implements StateCommand{
    private static InputReader in = new InputReader(System.in);
    private static String inp;

    @Override
    public Objects command(Objects object) {
        System.out.println("\nWhen do you bought it (In order DD-MM-YYYY): ");
        inp = in.next();
        object.setDate(inp);
        return object;
    }
}

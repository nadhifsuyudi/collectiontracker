package Action.Command;

import InputReader.InputReader;
import Model.Objects;

public class StoreRarity implements StateCommand{
    private static InputReader in = new InputReader(System.in);
    private static String inp;

    @Override
    public Objects command(Objects object) {
        System.out.println("\nNormal, Rare, Super Rare");
        System.out.println("\nWhat is the rarity of the object (Please input from list above) : ");
        inp = in.next();
        object.setRarity(inp);
        return object;
    }
}

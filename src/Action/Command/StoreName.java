package Action.Command;

import InputReader.InputReader;
import Model.Objects;

public class StoreName implements StateCommand{
    private static InputReader in = new InputReader(System.in);
    private static String inp;

    @Override
    public Objects command(Objects object) {
        System.out.println("\nWhat is the name of the object: ");
        inp = in.next();
        object.setName(inp);
        return object;
    }
}

package Action.Command;

import InputReader.InputReader;
import Model.Objects;

public class StoreCategory implements StateCommand {

    private static InputReader in = new InputReader(System.in);
    private static String inp;

    @Override
    public Objects command(Objects object) {
        System.out.println("\nWhat is the category of the object: ");
        inp = in.next();
        object.setCategory(inp);
        return object;
    }
}

package Action.Command;

import InputReader.InputReader;
import Model.Objects;

public class StorePlace implements StateCommand {
    private static InputReader in = new InputReader(System.in);
    private static String inp;

    @Override
    public Objects command(Objects object) {
        System.out.println("\nWhere do you bought it : ");
        inp = in.next();
        object.setPlace(inp);
        return object;
    }
}

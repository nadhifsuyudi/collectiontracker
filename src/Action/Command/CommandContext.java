package Action.Command;


import Model.Objects;

public class CommandContext implements StateCommand{

    private StateCommand state;

    public void setState(StateCommand state) {
        this.state = state;
    }

    public StateCommand getState() {
        return state;
    }

    @Override
    public Objects command(Objects object) {
        this.state.command(object);
        return object;
    }
}

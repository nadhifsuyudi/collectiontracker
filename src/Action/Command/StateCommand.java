package Action.Command;

import Model.Objects;

public interface StateCommand {
    public Objects command(Objects object);

}

package Action.Command;

import InputReader.InputReader;
import Model.Objects;

public class StoreYear implements StateCommand{
    private static InputReader in = new InputReader(System.in);
    private static String inp;

    @Override
    public Objects command(Objects object) {
        System.out.println("\nWhen is the manufactured year : ");
        inp = in.next();
        object.setYear(Integer.parseInt(inp));
        return object;
    }
}

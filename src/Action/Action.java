package Action;

import Action.State.*;
import InputReader.InputReader;

import java.io.*;
import java.util.List;


public class Action {

    private InputReader in = new InputReader(System.in);
    private String inp;

    private Context current = new Context();
    private Store store = new Store();
    private View view = new View();
    private Modify modify = new Modify();
    private Delete delete = new Delete();
    private ViewAll viewAll = new ViewAll();
    private Help help = new Help();
    private ViewByRarity viewByRarity = new ViewByRarity();
    private ViewByCategory viewByCategory = new ViewByCategory();


    public Action() {
    }

    public String run() throws IOException {

        System.out.print("\nWhat do you want to do? (Insert 'help' to know all commands): ");
        inp = in.next();

        if(inp.equalsIgnoreCase("store")){
            current.setState(store);
            current.doAction();
            return "main";
        } else if(inp.equalsIgnoreCase("view")){
            current.setState(view);
            current.doAction();
            return "main";
        } else if (inp.equalsIgnoreCase("modify")){
            current.setState(modify);
            current.doAction();
            return "main";
        } else if (inp.equalsIgnoreCase("delete")){
            current.setState(delete);
            current.doAction();
            return "main";
        } else if (inp.equalsIgnoreCase("exit")){
            System.out.println("\nThank you for using this application!");
            return "exit";
        } else if (inp.equalsIgnoreCase("viewAll")){
            current.setState(viewAll);
            current.doAction();
            return "main";
        } else if (inp.equalsIgnoreCase("viewRarity")){
            current.setState(viewByRarity);
            current.doAction();
            return "main";
        } else if (inp.equalsIgnoreCase("viewCategory")){
            current.setState(viewByCategory);
            current.doAction();
            return "main";
        } else if(inp.equalsIgnoreCase("help")){
            current.setState(help);
            current.doAction();
            return "main";
        } else {
            System.out.println("\nThere is no command like that on this application!");
            current.setState(help);
            current.doAction();
            return "main";
        }
    }
}

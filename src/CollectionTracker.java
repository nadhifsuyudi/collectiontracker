import java.io.IOException;

import Action.Action;


public class CollectionTracker {

    private static Action actionCmd;
    private static String status;


    public static void main(String[] args) throws IOException {

        System.out.println("\n=============================");
        System.out.println(" Welcome to CollectionTracker!! ");
        System.out.println("=============================\n");

        status = "main";
        actionCmd = new Action();
        do{
            status = actionCmd.run();
        }while(status.equals("main"));

    }
}

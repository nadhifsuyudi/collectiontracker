package Test;

import Model.Objects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class TestView {
    private static String inp;
    public static void main(String[] args) throws IOException {
        try{
            URL path = TestStore.class.getResource("TestViewInput.txt");
            File file = new File( path.getFile());
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            Objects testObj = new Objects();

            System.out.println("Testing store features....");
            inp = bufferedReader.readLine();

            if (inp.equalsIgnoreCase("store")){
                System.out.println("Store command has been selected...");
            } else {
                System.out.println("Wrong command has been selected...");
                System.exit(-1);
            }

            testObj.setName(inp = bufferedReader.readLine());
            testObj.setDate(inp = bufferedReader.readLine());
            testObj.setYear(Integer.parseInt(inp = bufferedReader.readLine()));
            testObj.setRarity(inp = bufferedReader.readLine());
            testObj.setPlace(inp = bufferedReader.readLine());
            testObj.setCategory(inp = bufferedReader.readLine());

            System.out.println("The object has been stored!");
            inp = bufferedReader.readLine();

            if (inp.equalsIgnoreCase("view")){
                System.out.println("View command has been selected...");
                inp = bufferedReader.readLine();
                if (testObj.getName().equalsIgnoreCase(inp)){
                    System.out.println("Target object is exists...");
                }
            } else {
                System.out.println("Wrong command has been selected...");
                System.exit(-1);
            }

            System.out.println("The object has been viewed!");
            inp = bufferedReader.readLine();

            if (inp.equalsIgnoreCase("exit")){
                System.out.println("Exit command has been selected...");
                System.out.println("...This test has run succesfully!\n");
            } else {
                System.out.println("Wrong command has been selected...");
                System.exit(-1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

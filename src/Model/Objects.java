package Model;

public class Objects{
    private String name;
    private String date;
    private int year;
    private String rarity;
    private String place;
    private String category;

    public Objects() {};

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getRarity() {
        return rarity;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPlace() {
        return place;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public String toString(){
        return "Name: " + name + "\nDate: "+ date + "\nYear: "+ year +
                "\nRarity: "+ rarity + "\nPlace: "+ place + "\nCategory: "+ category;
    }
}
package Repository;

import Model.Objects;

import java.util.HashMap;

public class AllObject{
    private static HashMap<String, Objects> repo = new HashMap<>();

    public static void add(Objects object) {
        repo.put(object.getName(), object);
    }

    public static Objects search(String target){
        if(repo.get(target)==null){
            System.out.println("That object isn't exists!");
            return null;
        } else{
            return repo.get(target);
        }

    }

    public static void remove(String target){
        repo.remove(target);
    }

    public static HashMap<String, Objects> getRepo() {
        return repo;
    }
}
